## Le choix d'un éditeur de code

Un bon éditeur de code n'est pas un éditeur de texte. En effet, l'éditeur de code doit comporter les fonctionnalités suivantes.

* Indentation automatique (on ne code pas tout aligné à gauche!)
* Coloration du code
* Compilation dans l'éditeur avec lien direct sur les erreurs. Le terminal est bien pour apprendre les commandes, le niveau suivant est de gagner en efficacité.
* Les plus quasiment indispensables d'un éditeur :
  * autocomplétion lors de la frappe,
  * recherche de définition de fonctions,
  * renommage de variable ou de fonction (refactoring) dans tout le projet,
  * un copilot,
  * etc.

L'objectif est que vous trouviez l'éditeur qui vous convient et surtout que vous soyez efficace avec. Choisissez parmi ces 6.

* **[VisualStudioCode/VSCode](./vscode.md)**  l'éditeur qui devient la référence, multi-plateforme (Linux, Windows, MacOS). C'est le couteau suisse du développement, car avec ses extensions il sait tout faire, mais il faut bien choisir ses plugins. De nombreux avantages (plugins, rapides si votre machine est puissante, légers, multi-plateformes). Défauts : il faut le configurer et de nombreuses extensions à s'y perdre.

* **[Visual Studio](./visualstudio.md)** : très bien, mais uniquement sous Windows. Si vous êtes Windows, il faut le connaître (au moins pour votre CV) !

* **[XCode](./xcode.md)**  : très bien, mais seulement si vous êtes sous Max. Si vous êtes sous Mac, il faut !

* **[CodeBlocks](./codeblocks.md)** : Linux/Windows, bon compromis entre simple et complet, mais un peu vieillissant. Le débogueur sous Windows est souvent cassé. Il est installé sur les machines de l'université. Mais un peu vieillissant.

* **[CLion](https://www.jetbrains.com/clion/)** : Linux/Windows/Mac, bon compromis entre simple et complet. Gratuit pour les étudiants. Mais il faut s'enregistrer. Attention, il n'est pas installé sur les PCs de l'université.

* **[QtCreator](http://qt-project.org/wiki/Category:Tools::QtCreator|QtCreator)** : très bien si vous utilisez Qt, mais pas que ...


**Remarques : Apprenez et utilisez les raccourcis clavier de votre éditeur de code. Il peut y avoir des questions là-dessus lors du contrôle :-) ** Vous devez rapidement ne plus toucher à votre souris 8-o, le gain de temps est considérable. Prenez le temps de bien choisir votre éditeur.
# XCode

Xcode est un environnement de développement intégré (IDE) développé par Apple, spécifiquement conçu pour le développement d'applications destinées aux systèmes d'exploitation iOS, macOS, watchOS et tvOS. Il offre un ensemble complet d'outils pour la création, la conception, le débogage et la distribution d'applications pour les produits Apple.

## XCode et CMake

CMake permet de générer un projet pour XCode comme ceci 

```
cd mon projet
mkdir build
cd build
cmake -G Xcode ..
open MonProjet.xcodeproj
```

Dans le cadre de cette UE c'est vers CMake qu'il faut regarder d'abord.


## XCode tuto

???